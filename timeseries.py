import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px


def temporal_timeseries_by_station(df, condition_str):

    stations = df['station_id'].unique()
    fig, ax = plt.subplots(nrows=len(stations), ncols=1, figsize=(20, 10), sharex=True)
    for i, station in enumerate(stations):
        dataset = df[(df['station_id'] == station)]
        ax[i].plot(pd.to_numeric(dataset['time']), np.array(dataset['PurpleAir'] * -1), label='PurpleAir')
        ax[i].plot(pd.to_numeric(dataset['time']), np.array(dataset['persistence'] * -1), label='persistence')
        ax[i].set_title(station)
        ax[i].set_xlabel('gt time')
        ax[i].set_ylabel('absolute RMSE value')
        ax[i].legend()

    fig.suptitle(f'timeseries where station mean value is {condition_str}')
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.savefig(f'figures/timeseries_{condition_str}')
    plt.close(fig)


def plotly_timeseries_by_station(df, condition_str, separate_stations=True, rmse=False, baqi=False):
    if rmse:
        df[['PurpleAir', 'persistence']] *= -1  # a fix for -RMSE
        value_name = 'absolute RMSE value'
        value_vars = ['PurpleAir', 'persistence', 'Diff']
        if baqi:
            title = f'timeseries where the difference between PA RMSE to GT and pesistence RMSE to GT {condition_str} calculated in BAQI'
        else:
            title = f'timeseries where the difference between PA RMSE to GT and pesistence RMSE to GT {condition_str} calculated in concentration'
    elif baqi:
        value_name = 'BAQI category'
        title = f'timeseries of raw values where {condition_str} in BAQI'
        value_vars = ['gt_baqi', 'PurpleAir', 'persistence', 'Diff']
    else:
        value_name = 'concentration value'
        title = f'timeseries of raw values where {condition_str} in concentration'
        value_vars = ['gt_value', 'PurpleAir', 'persistence', 'Diff']

    melted = pd.melt(df, id_vars=['station_id', 'time'], value_vars=value_vars,
                     var_name='model name', value_name=value_name)
    if separate_stations:
        fig = px.line(melted, x='time', y=value_name, color='model name',
                      facet_col='station_id', facet_col_wrap=int(np.ceil(melted['station_id'].nunique()/6)),
                      title=title)
    else:
        fig = px.scatter(melted, x='time', y=value_name, color='station_id',
                         facet_row='model name',
                      title=title)
    fig.write_html(f'figures/timeseries_{condition_str}_rmse={rmse}_baqi={baqi}.html')


def plot_baqi_histogram(piv_df):
    # this was done using the non-abs pivoted df for BAQI.
    # it can also be generated using real values and recalculating the score
    melt_df = piv_df.melt(id_vars=['station_id', 'time'], value_vars=['PurpleAir', 'persistence'],
                          var_name='model name', value_name='BAQI diff')

    fig = px.histogram(melt_df, x='BAQI diff', color='model name', barmode='group', histnorm='percent',
                       range_y=[0.0001, 0.12], log_y=True, title='percent of data in BAQI category difference - positive = catchup above GT')
    fig.update_layout(yaxis_title='percent of data (out of 100) in %')
    fig.write_html(f'figures/hist_baqi_diff_logy.html')
