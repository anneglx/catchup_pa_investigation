import os
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from pa_data_prepare import groupby_describe


def plot_map_scatter(df,
                     location_id='station_id', lat='lat', lon='lon',
                     mapbox_token=None,
                     title=None,
                     size_param=None,
                     color_param=None,
                     symbol_group=None,
                     show=False, output_path=False,
                     color_scale='RdBu',
                     fig_title='worldmap',
                     cmid=0,
                     mbstyle ="open-street-map",
                     cmin=None,
                     cmax=None):
    """
    @param df:  dataframe to plot from, must include latitude, longitude and location identifying columns
    @type df: pandas DataFrame
    @param location_id: name of column with unique location name or id
    @type location_id: string
    @param lat: latitude column name
    @type lat: str
    @param lon: longitude column name
    @type lon: str
    @param mapbox_token: mapbox token to load background maps
    @type mapbox_token: str
    @param title: plot title
    @type title: str
    @param size_param: name of column with integers corralated with marker size
    @type size_param: str
    @param color_param: name of column to categorize the colors by
    @type color_param: str
    @param show: whether to show the figure after creating it
    @type show: bool
    @param output_path: path to output directory
    @type output_path: str
    @param fig_title: if out_path is True, enter output file name without the extension
    @type fig_title: str
    """
    hover_data = []
    for item in [location_id, color_param, size_param]:
        hover_data.append(item)
    customization = {'title': title,
                     'color': color_param,
                     'size': size_param,
                     # data parameters
                     'lat': lat,
                     'lon': lon,
                     # visual parameters currently hard-coded
                     'zoom': 2,
                     'color_continuous_scale': color_scale,
                     'color_continuous_midpoint': cmid,
                     'symbol': symbol_group
                     }
    plot_kwargs = {k: v for k, v in customization.items() if v is not None}
    plot_kwargs['data_frame'] = df
    # plotting scattered counter values on map from mapbox, marker size is the counter (perhaps a horrible mistake)
    map_fig = px.scatter_mapbox(**plot_kwargs)
    if mapbox_token:
        px.set_mapbox_access_token(mapbox_token)
        map_fig.update_layout(mapbox_style='carto-darkmatter') #'dark'
    else:
        map_fig.update_layout(mapbox_style=mbstyle)
    # saving the figure
    if cmax:
        map_fig.update_traces(marker_cmax=cmax, marker_cmin=cmin, selector=dict(type='scattermapbox'))
    if show:
        map_fig.show()
    if output_path:
        map_fig.write_html(os.path.join(output_path, f'{fig_title}_worldmap.html'))
    return map_fig


def legend_bisect_condition(pivoted_df, col, legend_labels_list, conditions=False):
    """
    get a filtered and pivoted df,
    assign legend values to groups of negative or non-negative values in provided column (col)
    legend_labels_list is a list with the labels;
    condition_kwargs are the arguments passed to a numpy function that returns elements chosen from `x` or `y` depending on `condition`, where True, yield `x`, otherwise yield `y`.
    """
    # if not conditions:
    #     condition_kwargs = {'condition': pivoted_df[col] >= 0,
    #                          'x': legend_labels_list[0],
    #                          'y': legend_labels_list[1]}
    # else:
    #     condition_kwargs = conditions
    # assign labels to negative and positive values:
    pivoted_df["legend"] = np.where(condition=pivoted_df[col] >= 0, x=legend_labels_list[0], y=legend_labels_list[1])
    return pivoted_df


def plot_histogram_bisect(pivoted_df,
                          col,
                          group_id='station_id',
                          show=True,
                          output_path=None,
                          hist_norm='percent',
                          fig_title='hist',
                          express=True):
    """
    plot histogram of pivoted dataframe, by labels in 'legend' column
    @param pivoted_df: prepared dataframe
    @type pivoted_df: pandas.DataFrame
    @param col: name of column with values of interest
    @type col: str
    @param group_id: name of column for x axis groups
    @type group_id: str
    @param show: whether to show figure, False by default
    @type show: bool
    @param output_path: path to output directory
    @type output_path: str
    @param hist_norm: histogram normalization type
    @type hist_norm: str
    @param fig_title: name for the output figure (optional if output_path is None)
    @type fig_title: str
    @param express: whether to use plotly.express plotting method
    @type express: bool
    @return: creates plot figure
    """
    legend_labels_list = pivoted_df['legend'].unique()
    colors = {legend_labels_list[1]: 'red',
              legend_labels_list[0]: 'lightgreen'}

    #partial dataframe of values in COL and legend per station/hour
    if not express:
        fig_diff_np = go.Figure()
        for c in col:
            partial_df = pivoted_df.copy()[[group_id, c, 'legend']]
            for label in legend_labels_list:
                fig_diff_np.add_trace(go.Histogram(
                    x=partial_df[group_id],
                    histnorm=hist_norm,
                    name=label,
                    marker_color=colors[label],
                    opacity=0.75))
        if show:
            fig_diff_np.show()
        elif output_path:
            fig_diff_np.write_html(os.path.join(output_path, f'{fig_title}.html'))
    # similarly with plotly express group_mode

    else:
        hist = px.histogram(pivoted_df, x=group_id, color="legend", color_discrete_map=colors)
        hist.update_layout(barmode='group')
        if show:
            hist.show()
        if output_path:
            hist.write_html(os.path.join(output_path, f'{fig_title}.html'))


def plot_describe_express_maps(df, stats_col,
                            location_id='station_id',
                            groupby_cols = ['station_id'],
                            output_path=None,
                            show=True,
                            map_token='',
                            size_col = None,
                            title='default plot title',
                            fig_title='describe',
                            color_scale='RdBu_r'):
    """
    plot stats (from describe method) on world map
    @param df: data frame of pivoted data frame
    @param stats_col: column to describe
    @param location_id: name of location id column, i.e station_id, lat_lon, local_id etc.
    @type location_id: str
    @param groupby_cols: names of columns to group by
    @type groupby_cols: list
    @param output_path: path to output directory
    @type output_path: str
    @param show: whether to show figure
    @type show: bool
    @param map_token: mapbox token
    @type map_token: str
    @param size_col: column to set size by
    @type size_col: column to set size by
    @param title: plot title
    @type title: str
    @param fig_title: figure output file prefix
    @type fig_title: str
    @param color_scale: plotly continuous color scale
    @type color_scale: str
    @return: scatter plotting on mapbox map
    @rtype: object
    """
    col_list, all_cols_desc = groupby_describe(df, groupby_cols, stats_col)
    for c in col_list:
        p = c.split(".")[1]
        max_val = np.max(all_cols_desc[c]) / 2
        min_val = np.min(all_cols_desc[c]) / 2
        mid_val = (max_val - min_val) / 2
        c_scale = color_scale
        plot_title = title + f', {stats_col} describe: {p}'

        # adjust scale to stats type
        if p == 'std':
            min_val = 0
            c_scale = 'rainbow' #'solar'

        elif p == 'mean':
            mid_val = 0

        elif p == '25%':
            mid_val = None
            continue

        elif p == '75%':
            mid_val = None
            continue

        elif p == 'min':
            mid_val = None
            c_scale = 'rainbow_r'

        elif p == 'max':
            mid_val = None
            min_val = -1
            c_scale = 'rainbow'

        elif p == 'count':
            min_val = 0
            continue

        map_element = plot_map_scatter(df=all_cols_desc, mapbox_token=map_token,
                                       location_id=location_id, lat='lat', lon='lon',
                                       color_scale=c_scale,
                                       cmin=min_val, cmax=max_val,
                                       cmid=mid_val,
                                       color_param=c, title=plot_title, size_param=f'{stats_col}.{size_col}')
        if output_path:
            map_element.write_html(os.path.join(output_path, f'{fig_title}_{c.split(".")[0]}_{c.split(".")[1]}_map.html'))
        if show:
            map_element.show()


# WIP
def counter_bygroup(pivoted_df, col, group_id=['station_id']):
    """
    WIP
    @param pivoted_df:
    @type pivoted_df:
    @return:
    @rtype:
    """
    # key = new column name, value = aggfunction
    named_aggfunc_args = {
        f'{col}_counter': pd.NamedAgg(column=col, aggfunc="count"),
        'lat': pd.NamedAgg(column="lat", aggfunc='mean'),
        'lon': pd.NamedAgg(column="lon", aggfunc=np.mean).reset_index()
    }
    # count values in labeled groups, per group_id
    grpby = group_id + ['legend']
    p = pivoted_df.groupby(grpby).agg(**named_aggfunc_args)



if __name__=='__main__':
    # breezometer's authenticatio token #todo: remove
    tkn = 'pk.eyJ1IjoiYW5uZS1icnoiLCJhIjoiY2t3OHd3ZzNhMXBwYzJybWU5YnZxMHFrYyJ9.RGGVYWIwZTh-tAbvJUiRXQ'

    current_dir = os.getcwd()
    pivoted_df = pd.read_csv(os.path.join(current_dir, 'pivoted_oct_catchup.csv'))
    # example call of plot_describe_express_maps
    plot_describe_express_maps(df=pivoted_df,
                                stats_col='Diff',
                                title='oct_catchup_pa-persistence',
                                location_id='station_id',
                                groupby_cols=['station_id', 'lat', 'lon'],
                                output_path=current_dir,
                                size_col='count',
                                color_scale='RdBu_r',
                                show=True,
                                map_token=tkn,
                                fig_title='_baqi_oct_catchup_pa-persistence')

    # prepare bisected legend by condition
    piv_df = pivoted_df.copy()
    # remove all rows with no difference in category error
    piv_df = piv_df.loc[piv_df['Diff'] != 0]
    piv_df["legend"] = np.where(piv_df['Diff'] > 0, 'Persistence smaller MAE', 'PA smaller MAE')
    # example use of function plot_histogram_bisect
    plot_histogram_bisect(piv_df,
                            col=['Diff'],
                            group_id='station_id',
                            show=True,
                            hist_norm='percent',
                            fig_title='_baqi_hist',
                            output_path=current_dir,
                            express=True)

    # combine close stations within 100 meters
    piv_df['lat_lon'] = (piv_df['lat']*1000).astype(int).astype(str)+'_'+(piv_df['lon']*1000).astype(int).astype(str)
    # example use of groupby_describe and plot_map_scatter
    col_list, bsect_df = groupby_describe(piv_df, groupby_cols=['lat_lon', 'legend'], stats_col='Diff', partial=False)
    bmap = plot_map_scatter(df=bsect_df,
                     location_id='lat_lon', lat='lat.mean', lon='lon.mean',
                     mapbox_token=tkn,
                     title="Diff = |RMSE(PA)| - |RMSE(Persistence)|",
                     size_param='Diff.count',
                     color_param='legend',
                     show=True,
                     output_path=os.path.join(current_dir, f'_baqi_bisection_map.html'),
                     color_scale='BuPu',
                     fig_title='worldmap',
                     cmid=None,
                     mbstyle="open-street-map",
                     cmin=None,
                     cmax=None)

    # plot bisection histogram
    bmap_hist = px.histogram(bsect_df, x="legend", color='legend', color_discrete_sequence=['lightblue', 'indianred'])
    bmap_hist.write_html(os.path.join(current_dir, f'_baqi_bisection_histogram.html'))

