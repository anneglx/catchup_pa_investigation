import json
import os
import argparse
import numpy as np
import pandas as pd
import time
from datetime import datetime
from timeseries import temporal_timeseries_by_station, plotly_timeseries_by_station


BAQI_LINES = {'pm25': [0, 25, 45, 110, 500]}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--measurements',
                        type=str,
                        help=('path to measurements bq file'),
                        required=False,
                        default='./bq-oct_measurements.csv')
    parser.add_argument('--catchup',
                        type=str,
                        help=('path to catchup csv file'),
                        required=False,
                        default='./oct_catchup.csv')
    parser.add_argument('--CATCHUP_DIR',
                        type=str,
                        help=('path to directory of catchup json files'),
                        required=False,
                        default='./catchup_oct_files/')
    parser.add_argument('--OUTPUT_DIR', type=str,
                        help=('path to directory where figures will be saved'),
                        required=False,
                        default='./figures/')
    return parser.parse_args()


def get_measurements_data(filename):
    '''
    takes a CSV with RDS data, resamples only valid data by lat_lon * 10^4 and by pollutant.
    :param filename: the csv file name of the RDS measurements
    :return: measurements: df. the results with the columns: 'station_id' (lat_lon as string), 'pollutant_name',
                            'time' (as hourly datetime), 'gt_value' (concentrations)
    '''
    measurements = pd.read_csv(filename)
    # take only valid data
    measurements = measurements[measurements.valid]
    # create a lat_lon column as station id
    measurements['station_id'] = (measurements.latitude * 10000).astype(int).astype(str) + '_' + (measurements.longitude * 10000).astype(int).astype(str)
    # filter only relevant columns
    measurements.rename(columns={'name': 'pollutant_name', 'value': 'gt_value'}, inplace=True)
    measurements = measurements[['station_id', 'pollutant_name', 'time', 'gt_value']]
    # resampling for each station and pollutant for a round hour
    measurements['time'] = pd.to_datetime(measurements['time'], utc=True)
    measurements.set_index('time', inplace=True)
    measurements = measurements.groupby(['station_id', 'pollutant_name']).resample('H').mean()
    measurements.reset_index(inplace=True)
    return measurements


def load_catchup_json_file(json_path):
    with open(json_path, 'rb') as f:
        # json file to DF
        j = json.load(f)['debug']
        json_df = pd.json_normalize(j)
        json_df = json_df.loc[json_df['pollutant_name'] == 'pm25']
        all_results_cols = [col for col in json_df.columns if col[:11] == 'all_results']
        exploded_df = json_df.explode(all_results_cols)
        t = os.path.basename(json_path).split('.')[0].replace('_', ':')
        exploded_df['time'] = pd.to_datetime(t, utc=True)
    return exploded_df


def prepare_pa_catchup(folder_path=None, output_csv='oct_catchup.csv', ignore_models=['ml_pd', 'ml_npd', 'cams']):
    '''
    given already downloaded data from gs://catchup-generated-functions/predict_cat_data for the relevant dates,
    loads and combines the data into a df with pivoted data for each location and hour with purpleair prediction.
    saves the data to a CSV file for future use.
    :param folder_path: the folder path to read the json files from
    :param output_csv: if exists, the path to an already written csv file
    :param ignore_models: a list of all the models to ignore in the comparison
    :return all_rcd: returns the data in df form
    '''
    try:  # if data already was processed and saved as csv, use it
        #TODO: add an input option to choose whether to use a CSV if exists or use json data.
        all_rcd = pd.read_csv(output_csv)
        all_rcd[~all_rcd["all_results.model_name"].isin(ignore_models)]
        all_rcd.time = pd.to_datetime(all_rcd['time'], utc=True)
    except:
        os.makedirs(folder_path, exist_ok=True)
        raw_catchup_data = os.listdir(folder_path)

        # prepare each json
        filtered_list = list()
        print(datetime.utcnow().isoformat(), 'Start loading catchup data')
        for rcd in raw_catchup_data:
            exploded_df = load_catchup_json_file(os.path.join(folder_path, rcd))
            # remove unused models
            filtered_df = exploded_df[~exploded_df["all_results.model_name"].isin(ignore_models)]
            # take only locations with PA sensors
            df_PA = filtered_df[filtered_df['all_results.model_name'] == 'PurpleAir'].dropna(subset=['all_results.value'])
            filtered_df = filtered_df[filtered_df['station_id'].isin(df_PA['station_id'].unique())]
            filtered_list.append(filtered_df)
        # union of all catchup filtered results
        all_rcd = pd.concat(filtered_list)
        all_rcd.to_csv(output_csv, index=False)
    return all_rcd


def to_category(dataset, pol):
    category_dataset = dataset.copy()
    category_dataset[dataset < 0] = np.nan
    for i, val in enumerate(BAQI_LINES[pol]):
        category_dataset[dataset > val] = i
    return category_dataset


def add_baqi(merged_df):
    # since we're only using om25 anyway, there's no point in separating by pollutant
    merged_df['all_results.baqi'] = to_category(merged_df['all_results.value'], 'pm25')
    merged_df['gt_baqi'] = to_category(merged_df['gt_value'], 'pm25')
    return merged_df


def merge_catchup_measurements(measurements_df, catchup_df, baqi=False, real_values=False):
    '''
    take 2 dfs, ranks the data by original score, pivots by model and calculates the absolute diff between PA and
    persistence.
    :param measurements_df: df created with get_measurements_data
    :param catchup_df: df created with prepare_pa_catchup
    :return pivoted_df: the final merged df.
    '''
    merged_df = pd.merge(left=catchup_df, right=measurements_df, how='inner',
                         on=['station_id', 'pollutant_name', 'time'])
    merged_df['gt_score'] = - abs(merged_df['gt_value'] - merged_df['all_results.value'])
    ranked_df = merged_df.copy()
    ranked_df.loc[ranked_df["all_results.model_name"] == "PurpleAir", "all_results.score"] = 1
    ranked_df["rank"] = ranked_df.groupby(["station_id", "time"])["all_results.score"].rank("dense", ascending=False)

    # pivot table and calculate difference between PA errors and other errors
    pivoted_df = ranked_df[ranked_df['rank'].isin([1, 2])].copy()  # todo: validate/fix - take only third place when cams in second place
    # there's no 'cams' column because the value is always None
    if baqi:
        pivoted_df = add_baqi(pivoted_df)
        pivoted_df['gt_baqi_score'] = - abs(pivoted_df['gt_baqi'] - pivoted_df['all_results.baqi'])
        values = ['all_results.baqi' if real_values else 'gt_baqi_score']
        pivoted_df = pd.pivot_table(data=pivoted_df,
                                    index=['station_id', 'lat', 'lon', 'pollutant_name', 'gt_baqi', 'time'],
                                    columns='all_results.model_name',
                                    values=values[0]).reset_index().sort_values(['station_id', 'time'])
    else:
        values = ['all_results.value' if real_values else 'gt_score']
        pivoted_df = pd.pivot_table(data=pivoted_df,
                                    index=['station_id', 'lat', 'lon', 'pollutant_name', 'gt_value', 'time'],
                                    columns='all_results.model_name',
                                    values=values[0]).reset_index().sort_values(['station_id', 'time'])
    # TODO: fix to take the relevant first and second places regardless of the name of the model
    pivoted_df['Diff'] = abs(pivoted_df['PurpleAir']).sub(abs(pivoted_df['persistence']))

    return pivoted_df


def groupby_describe(df, groupby_cols, stats_col, partial=True):
    """

    @param df: dataframe to describe
    @type df: pandas.DataFrame
    @param groupby_cols: names of columns to group by
    @type groupby_cols: list
    @param stats_col: column to describe
    @type stats_col: str
    @param partial: whether to return only all columns
    @return: list of columns related to describe.
    @rtype:
    """
    grouped_errors = df.groupby(groupby_cols)

    # describe() results
    all_columns_desc = grouped_errors.describe()
    # all_columns_desc.columns = [f'{i}.{j}' if j != '' else f'{i}' for i, j in all_columns_desc.columns]
    all_columns_desc.columns = all_columns_desc.columns.map('.'.join).str.strip('.')
    col_list = [x for x in all_columns_desc.columns if x.split('.')[0] == stats_col]
    if partial:
        desc_df = all_columns_desc[col_list]
        return col_list, desc_df.reset_index()
    else:
        pass
    return col_list, all_columns_desc.reset_index()


if __name__=='__main__':
    args = vars(parse_args())
    measurements = get_measurements_data(args['measurements'])
    time_catchup = time.time()
    catchup_data = prepare_pa_catchup()
    time_elapsed = time.time() - time_catchup
    print(f'total elapsed time for data aggregation: {time_elapsed}')
    final_data = merge_catchup_measurements(measurements, catchup_data)
    final_raw = merge_catchup_measurements(measurements, catchup_data, real_values=True)
    # data statistics
    all_describe = final_data.describe()
    print(f'{all_describe=}')
    _, described = groupby_describe(final_data, ['station_id'], 'Diff', partial=True)
    # plot stuff
    condition_str = 'mean >= 5'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.mean'] >= 5]['station_id'])].copy()
    # temporal_timeseries_by_station(timeseries_data, condition_str)
    plotly_timeseries_by_station(timeseries_data, condition_str)
    condition_str = 'count < 20'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.count'] < 20]['station_id'])].copy()
    plotly_timeseries_by_station(timeseries_data, condition_str, separate_stations=False)
    condition_str = 'std >= 10'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.std'] >= 10]['station_id'])].copy()
    plotly_timeseries_by_station(timeseries_data, condition_str)

    # do again but for BAQI:
    final_data = merge_catchup_measurements(measurements, catchup_data, baqi=True)
    final_raw = merge_catchup_measurements(measurements, catchup_data, baqi=True, real_values=True)
    # data statistics
    all_describe_baqi = final_data.describe()
    print(f'{all_describe_baqi=}')
    _, described = groupby_describe(final_data, ['station_id'], 'Diff', partial=True)
    # plot stuff
    condition_str = 'mean >= 0.05'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.mean'] >= 0.05]['station_id'])].copy()
    # temporal_timeseries_by_station(timeseries_data, condition_str)
    plotly_timeseries_by_station(timeseries_data, condition_str, baqi=True)
    condition_str = 'count < 20'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.count'] < 20]['station_id'])].copy()
    plotly_timeseries_by_station(timeseries_data, condition_str, separate_stations=False, baqi=True)
    condition_str = 'std >= 0.5'
    timeseries_data = final_raw[final_raw['station_id'].isin(described[described['Diff.std'] >= 0.5]['station_id'])].copy()
    plotly_timeseries_by_station(timeseries_data, condition_str, baqi=True)
