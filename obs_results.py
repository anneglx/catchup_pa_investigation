import pandas as pd
import numpy as np
import os
from datetime import timedelta
import pandasql as ps

# TODO: def rolling_dif():

def moving_baqi_diff(df, group_keys, stats_col='gt_baqi', window='3h', time_col='time'):
    """
    @param df: dataframe to describe
    @type df: pandas.DataFrame
    @param groupby_cols: names of columns to group by
    @type groupby_cols: list
    @param stats_col: column to calculate the moving difference on
    @type stats_col: str
    @param partial: whether to return only all columns
    @return: list of columns related to describe.
    @rtype:
    """

    stations_list = df[group_keys].unique()
    df[time_col] = pd.to_datetime(df[time_col])
    lst = list()
    for station in stations_list:
        dfs = df[df[group_keys] == station].copy()
        dfs.sort_values(by=time_col, inplace=True)
        dfs.set_index(time_col, inplace=True)
        dfs = dfs.resample('h').mean()
        tst_subject = dfs[[stats_col]]
        tst_subject = tst_subject.rolling(window).agg({stats_col: [np.nanmin, np.nanmax]})
        tst_subject.columns = ['.'.join(col).strip() for col in tst_subject.columns.values]
        tst_subject[group_keys] = station
        tst_subject['rolling_diff'] = tst_subject[f'{stats_col}.nanmax'] - tst_subject[f'{stats_col}.nanmin']
        lst.append(tst_subject)
    test_df = pd.concat(lst)
    test_df.reset_index(inplace=True)
    incidents = test_df[test_df['rolling_diff'] >= 2].copy()  # the last hour of the 3
    mid_incidents = incidents.copy()
    mid_incidents['time'] -= timedelta(hours=1)
    start_incidents = incidents.copy()
    start_incidents['time'] -= timedelta(hours=2)
    all_incidents = pd.concat([start_incidents, mid_incidents, incidents])
    full_incidents = pd.merge(left=all_incidents, right=df, how='left', on=['station_id', 'time'])
    full_incidents.sort_values(by='rolling_diff', axis=0, inplace=True)
    full_incidents.drop_duplicates(subset=['station_id', 'time'], keep="last", inplace=True)
    full_incidents.sort_values(by=['station_id', 'time'], axis=0, inplace=True)
    return full_incidents

if __name__=='__main__':
    current_dir = os.getcwd()
    piv_df = pd.read_csv(os.path.join(current_dir, 'piv_df.csv'))
    df = moving_baqi_diff(df=piv_df, group_keys='station_id', time_col='time')